# Content

One of the major challenges of the [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) repository was that content, markup, styles, and even functionality were generally bundled up all together in the same files. 

To avoid those problems with this new repository, we should conceptualize our content as living in a separate data layer. 

Nuxt gives us a tool with first-class support for handling our content as Markdown, JSON, YAML, XML, and even CSV files. It's the [Nuxt content module](https://content.nuxtjs.org/). 

As much as is possible, reasonable, and practical, content should be managed using this system. 
