---
  title:  Software. Faster
  og_itle:  Software. Faster
  description: GitLab simplifies your DevSecOps so you can focus on what matters. Learn more!
  twitter_description: GitLab simplifies your DevSecOps so you can focus on what matters. Learn more!
  og_description: GitLab simplifies your DevSecOps so you can focus on what matters. Learn more!
  og_image: /nuxt-images/open-graph/open-graph-gitlab.png
  twitter_image: /nuxt-images/open-graph/open-graph-gitlab.png
  hero:
    title: |
      Software.
      Faster.
    subtitle: GitLab simplifies your DevSecOps so you can focus on what matters.
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 0
    image:
      url: /nuxt-images/software-faster/lines.svg
      alt: software-faster hero image
      aos_animation: zoom-out-left
      aos_duration: 1600
      aos_offset: 200
    button:
      href: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/software-faster&glm_content=default-saas-trial
      text: Get free trial
      data_ga_name: free trial
      data_ga_location: hero
    secondary_button:
      video_url: https://player.vimeo.com/video/779307258?h=59b06b0e99&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
      text: Watch video
      data_ga_name: watch video
      data_ga_location: hero
  customer_logos:
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Link to T-Mobile and GitLab webcast landing page"
        alt: "T-Mobile logo"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Link to Goldman Sachs customer case study
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Link to Cloud Native Computing Foundation customer case study
        alt: "Cloud Native logo"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: Link to Siemens customer case study
        alt: "Siemens logo"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Link to Nvidia customer case study
        alt: "Nvidia logo"
        url: /customers/Nvidia/
      - alt: UBS Logo
        image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        link_label: Link to UBS customer case study
  featured_content:
    cards:
      - header: Speed
        description: Deliver software faster. Automate your software delivery process so you can deliver value faster and quality code more often.
        link:
          href: /solutions/delivery-automation/
          text: Learn more
          data_ga_name: delivery automation
          data_ga_location: body
        icon: speed-gauge
      - header: Built in
        description: Built in security. Adopt DevSecOps practices with continuous software security assurance across every stage.
        link:
          href: /solutions/continuous-software-security-assurance/
          text: Learn more
          data_ga_name: continuous software security assurance
          data_ga_location: body
        icon: secure
      - header: Flexibility
        description: Accelerate your digital transformation. Reach your digital transformation objectives faster with a DevOps platform for your entire organization.
        link:
          href: /solutions/digital-transformation/
          text: Learn more
          data_ga_name: digital transformation
          data_ga_location: body
        icon: digital-transformation
    case_studies:
      - header: Achieve 5x faster deployment time
        description: |
          The world's most trusted hacker-powered security company, **HackerOne**, improved pipeline time, deployment speed, and developer efficiency with GitLab Ultimate.
        showcase_img:
          url: /nuxt-images/software-faster/hackerone-showcase.jpg
          alt: Person working on a computer with code - HackerOne
        logo_img:
          url: /nuxt-images/logos/hackerone-logo.png
          alt: HackerOne logo
        link:
          href: /customers/hackerone/
          text: Learn more
          data_ga_name: hackerone
          data_ga_location: body
      - header: Become 100% cloud at the speed of DevSecOps
        description: |
          Nasdaq has a vision of becoming 100% cloud. They are partnering with GitLab to get there.
        showcase_img:
          url: /nuxt-images/software-faster/nasdaq-showcase.jpg
          alt: Nasdaq logo on a window
        logo_img:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: Nasdaq logo
        link:
          video_url: https://player.vimeo.com/video/767082285?h=e76c380db4
          text: Watch the video
          data_ga_name: nasdaq
          data_ga_location: body
      - header: Release features 144x faster
        description: |
          Airbus Intelligence improved their workflow and code quality with single application CI. Feature releases went from 24 hours to 10 min.
        showcase_img:
          url: /nuxt-images/blogimages/airbus_cover_image.jpg
          alt: Airplane wing on flight - Airbus
        logo_img:
          url: /nuxt-images/software-faster/airbus-logo.jpg
          alt: Airbus logo
        link:
          href: /customers/airbus/
          text: Read their story
          data_ga_name: airbus
          data_ga_location: body
  devsecops:
    header: The DevSecOps Platform
    description: |
      Empower development, security, and operations teams to build better
      software, faster.
    link:
      text: Learn more
      data_ga_name: platform
      data_ga_location: body
    cards:
      - header: Better insights
        description: End-to-end visibility across the software delivery lifecycle.
        icon: case-study-alt
      - header: Greater efficiency
        description: Built-in support for automation and integrations with 3rd services.
        icon: principles
      - header: Improved collaboration
        description: One workflow that unites developer, security, and ops teams.
        icon: roles
      - header: Faster time to value
        description: Continuous improvement through accelerated feedback loops.
        icon: verification
  resources:
    title: More Resources
    cards:
      - header: What is GitLab?
        description: See how GitLab is leading the way.
        link:
          text: Watch now
          data_ga_name: watch video
          data_ga_lcoation: body
          icon:
            name: play-circle
            alt: Play Circle Icon
        icon: video
      - header: See what’s happening at GitLab
        description: Stay in the know about releases, applications, contributions, news, events, and more.
        link:
          href: /blog/
          text: Read the blog
          data_ga_name: read the blog
          data_ga_location: body
          icon:
            name: chevron-lg-right
            variant: product
            alt: Chevron Right Icon
        icon: blog
  by_industry_case_studies:
    title: More resources
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: 2022 Global DevSecOps Survey
        subtitle: See what we learned from over 5000 DevOps professionals about automation, security, AI, and more.
        image:
          url: /nuxt-images/software-faster/devsecops-survey.svg
          alt: devsecops survey icon
        button:
          href: /developer-survey/
          text: Read the survey
          data_ga_name: devsecops survey
          data_ga_location: body
        icon:
          name: doc-pencil-alt
          alt: Play Circle Icon
      - title: 'Cadence is Everything: 10x engineering organizations for 10x engineers.'
        subtitle: GitLab CEO and co-founder Sid Sijbrandij on the importance of cadence in engineering organizations.
        image:
          url: /nuxt-images/blogimages/athlinks_running.jpg
          alt: picture of people running marathon
        button:
          href: /blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/
          text: Read the blog
          data_ga_name: read the blog
          data_ga_location: body
        icon:
          name: blog
          alt: Play Circle Icon
