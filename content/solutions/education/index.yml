---
  title: GitLab for the education industry
  description: GitLab enables campuses to modernize, innovate, and secure quickly with an all-in-one platform where everyone can contribute.
  components:
    - name: 'solutions-hero'
      data:
        note:
          - GitLab for the education industry
        title: The future of software development starts here
        subtitle: Give everyone at your school the enterprise edge
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Try Ultimate for Free
          url: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/&glm_content=default-saas-trial

          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Contact sales
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
          icon:
            variant: product
            name: chevron-lg-right
        image:
          image_url: /nuxt-images/solutions/education/education_hero.jpeg
          image_url_mobile: /nuxt-images/solutions/education/education_hero.jpeg
          alt: ""
          bordered: true
    - name: 'education-case-study-carousel'
      data:
        case_studies:
          - logo_url: /nuxt-images/logos/uw-logo.svg
            institution_name: University of Washington
            quote:
              img_url: /nuxt-images/solutions/education/university_of_washington.jpg
              quote_text: Over the past two years, GitLab has been transformational for our organization here at UW. Your platform is fantastic!
              author: Aaron Timss
              author_title: Director of Information  Technology, University of Washington
            case_study_url: /customers/uw/
            data_ga_name: University of Washington case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/case-study-logos/logo_uni_surrey.svg
            institution_name: University of Surrey
            quote:
              img_url: /nuxt-images/solutions/education/university_of_surrey.jpg
              quote_text: We like that GitLab is a platform that provides a one-point solution for many things. It’s not just a repository; it has many features, including Wiki, Pages, analytics, a web interface, merge requests, CI/CD, and issue comments. You can collaborate in issues and merge requests. It’s a nice package overall.
              author: Kosta Polyzos
              author_title: Senior Systems Administrator, University of Surrey
            case_study_url: /customers/university-of-surrey
            data_ga_name: University of Surrey case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/victoria-university-wellington-logo.svg
            institution_name: Te Herenga Waka—Victoria University of Wellington
            quote:
              img_url: /nuxt-images/solutions/education/victoria.jpeg
              quote_text: When I heard about GitLab Self-Managed, it was a very clear choice. It was really only GitLab that fulfilled the requirements I had within the engineering project management courses. That and GitLab being one single product.
              author: Dr. James Quilty
              author_title: Director of Engineering, Te Herenga Waka—Victoria University of Wellington
            case_study_url: /customers/victoria_university
            data_ga_name: Victoria University of Wellington case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/deakin-university-logo.svg
            institution_name: Deakin University
            quote:
              img_url: /nuxt-images/solutions/education/deakin-case-study.jpg
              quote_text: One of the drivers for us to adopt GitLab was the number of different out-of-the-box security features that allowed us to replace other solutions and open source tools and therefore the skill sets that came along with them.
              author: Aaron Whitehand
              author_title: Director of Digital Enablement, Deakin University
            case_study_url: /customers/deakin-university
            data_ga_name: Deakin University case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/heriot-watt-logo.svg
            institution_name: Heriot Watt University
            quote:
              img_url: /nuxt-images/solutions/education/heriot.jpeg
              quote_text: GitLab has played an instrumental role in preparing students for professional practice, especially in terms of learning about source control, builds and best practices in agile development.
              author: Rob Stewart
              author_title: Assistant Professor Computer Science, Heriot Watt Unversity
            case_study_url: /customers/heriot_watt_university
            data_ga_name: Heriot Watt University case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/dublin-city-university-logo.svg
            institution_name: Dublin City University
            quote:
              img_url: /nuxt-images/solutions/education/dublin-city-uni.jpg
              quote_text: "It’s a big plus for DCU that they can offer GitLab as part of the training curriculum to ensure students are abreast with technology. It's excellent preparation for getting in the industry."
              author: Jacob Byrne
              author_title: Fourth-year student, Dublin City University
            case_study_url: /customers/dublin-city-university
            data_ga_name: Dublin City University case study
            data_ga_location: case study carousel
    - name: 'case-study-tabs'
      data:
        heading: Why should you choose GitLab?
        tabs:
          - tab: Schools
            title: Help your educational institution — whether it’s a college, university, high school, code school, or something in between — deliver software faster and more securely.
            text: With GitLab’s DevSecOps platform, you have everything you need to bring teams together to shorten cycle times, reduce costs, strengthen security, and increase productivity.
            cta:
              text: Talk to an expert
              url: /sales/
              data_ga_name: talk to an expert
              data_ga_location: schools tab
            cards:
              - title: Accelerate your digital transformation
                icon_name: slp-digital-transformation
                text: |
                  Make your digital transformation a success with a single application for the entire software delivery lifecycle. GitLab’s DevSecOps platform will help you:

                  - Modernize the way you create and deliver software
                  - Increase operational efficiencies
                  - Deliver better products faster
                  - Reduce security and compliance risk

                cta:
                  text: Learn more
                  url: /solutions/digital-transformation/
                  data_ga_name: digital transformation
                  data_ga_location: schools tab
              - title: Improve collaboration with a single platform
                icon_name: slp-collaboration-alt-4
                text: |
                  Help everyone — developers, operations, security, and even faculty and students — work together more efficiently with a DevSecOps platform designed for collaboration across distributed teams. With GitLab, you have:

                  - Project planning with transparent issues, epics, boards, milestones and more
                  - Robust version control and history
                  - Merge requests for reviewing and discussing code (and the results of tests and scans) before merging it
                cta:
                  text: Learn more
                  url: /platform/
                  data_ga_name: platform
                  data_ga_location: schools tab
              - title: Ensure security and compliance
                icon_name: slp-release
                text: |
                  Secure your data, research, applications, student coursework and everything else, all while ensuring compliance with GitLab’s DevSecOps platform. With a comprehensive set of built-in security scanners, dependency scanning to help you create a SBOM, and compliant pipelines to help you automate security and compliance policies, you can meet your requirements without sacrificing speed.
                cta:
                  text: Learn more
                  url: /solutions/dev-sec-ops/
                  data_ga_name: devsecops
                  data_ga_location: schools tab
          - tab: Teachers
            title: Join faculty around the world and give your students the enterprise edge by bringing GitLab into your classroom and research lab.
            text: GitLab is a single platform for project management, collaboration, source control management, git, automation, security, and much more. Because it is easy to use, flexible, and all in one place, it is the best choice for integrating industry practices on campus. The GitLab for Education Program gives free licenses of GitLab for teaching, learning, and research to qualified institutions around the world!
            cta:
              text: Get verified
              url: /solutions/education/join
              data_ga_name: get verified
              data_ga_location: teachers tab
            cards:
              - title: GitLab in the classroom
                icon_name: slp-user-laptop
                text: |
                  GitLab is used in dozens of disciplines — not just coding. By bringing GitLab to your classroom, not only will students gain an advantage by learning an industry-leading platform, they’ll learn project management, collaboration, version control, and operational workflows. GitLab can be used as the single source of truth for end-to-end student projects in any domain.

                  For domain-specific skills, GitLab’s DevSecOps features allow students to learn software development, infrastructure management, information technology, and security. Code testing, continuous integration, continuous development, and security testing are all in one place. Bring your students the tool where they can take their learning to the next level.
                cta:
                  text: Read the GitLab in Education Survey Results
                  url: /solutions/education/edu-survey
                  data_ga_name: gitlab education survey
                  data_ga_location: teachers tab
              - title: GitLab in research
                icon_name: slp-open-book
                text: |
                  GitLab is transforming the scientific research process across disciplines by bringing the DevSecOps paradigm to the scientific community. Researchers are finding increased transparency, collaboration, reproducibility, speed to results, and data integrity by integrating GitLab into their research process. Thousands of peer-reviewed papers have been published either about GitLab itself or with data stored in GitLab.
                cta:
                  text: See research examples
                  url: /blog/2022/02/15/devops-and-the-scientific-process-a-perfect-pairing/
                  data_ga_name: gitlab in research
                  data_ga_location: teachers tab
              - title: Invite us to your campus
                icon_name: slp-institution
                text: |
                  Our team would love to come help you or your students get started on your DevSecOps journey. We offer workshops, guest lectures, and can chat with your student orgs about DevOps, GitLab, and more.
                cta:
                  text: Request a visit
                  url: https://docs.google.com/forms/d/e/1FAIpQLSeGya0P_GOn9QXR0VrQYoy7uCSJ2x6IryrKEGBzcmQmcDZv1g/viewform
                  data_ga_name: invite us to your campus
                  data_ga_location: teachers tab
          - tab: Students
            title: Get an edge on the job market by learning industry-leading tools now. Leading companies around the world use GitLab to build software — faster and more securely — and you can too.
            text: Start building a portfolio of skills across the DevSecOps lifecycle with GitLab while you are in school. Start small and learn as you go. We are here to help.
            cta:
              text: Read customer stories
              url: /customers/
              data_ga_name: read case studies
              data_ga_location: students tab
            cards:
              - title: Join the GitLab Forum
                icon_name: slp-chat
                text: |
                  Come on by and introduce yourself! We created a special Education category as a resource for our program members. Consider the forum your resource for Q&A on GitLab!
                cta:
                  text: Go to Gitlab Forum
                  url: https://forum.gitlab.com/c/gitlab-for-education/37?_gl=1*8fqeup*_ga*MTc1MDg3NzQ5Ni4xNjU1NDAwMTk3*_ga_ENFH3X7M5Y*MTY3MTczOTQyMC4xOTQuMS4xNjcxNzQyMzI4LjAuMC4w
                  data_ga_name: gitlab forum
                  data_ga_location: students tab
              - title: Attend or host a meetup
                icon_name: slp-calendar-alt-3
                text: |
                  Check out our GitLab meetups! A great way to meet professionals in your area and learn about DevOps. Can’t find a meetup near you? Start one in your community. We make it super easy
                cta:
                  text: Find a local meetup
                  url: /community/meetups
                  data_ga_name: meetups
                  data_ga_location: students tab
              - title: Join a hackathon
                icon_name: slp-idea-collaboration
                text: |
                  We host quarterly hackathons where you can make contributions, win prizes, and network with other contributors. Students encouraged!
                cta:
                  text: Register for a hackathon
                  url: /community/hackathon/
                  data_ga_name: hackathon
                  data_ga_location: students tab
              - title: Getting started
                icon_name: slp-pencil
                text: |
                  Are you new to GitLab? Check our learning resources to start your DevSecOps journey.
                cta:
                  text: Learning resources
                  url: https://docs.gitlab.com/ee/tutorials/
                  data_ga_name: getting started
                  data_ga_location: students tab
              - title: Become a contributor
                icon_name: slp-code
                text: |
                  Are you a student looking to gain experience? Want to develop your skills wthin a support network? Check our our code contributor program to get started!
                cta:
                  text: Become a contributor
                  url: /community/contribute
                  data_ga_name: contribute
                  data_ga_location: students tab
    - name: 'education-feature-tiers'
      data:
        heading: Bring GitLab to your campus
        teacher_tier:
          title: Teachers
          text: Get free Education licenses to support teaching, learning, and research*
          features:
            - item: Unlimited seats
            - item: Ultimate tier
            - item: Any deployment method
            - item: 50,000 CI/CD minutes
            - item: Community support
            - item: Classroom or research use only
          cta:
            text: Get verified
            url: /solutions/education/join/
            data_ga_name: get verified
            data_ga_location: education feature tiers
          disclaimer: '*not authorized to run, administer, or operate an institution'
        schools_tier:
            title: Schools
            text: Access the complete DevSecOps platform for the whole campus for one simplified price
            features:
              - item: Unlimited seats**
              - item: Ultimate tier
              - item: Any deployment method
              - item: 50,000 CI/CD minutes
              - item: Priority support
              - item: No use case restrictions
            cta:
              text: Contact sales
              url: /sales/
              data_ga_name: contact sales
              data_ga_location: education feature tiers
            disclaimer: '**up to student enrollment'
            callout_box:
              title: Or, tailor a plan to your specific needs
              features:
                - item: Just the seats you need
                - item: Any tier
                - item: Any deployment method
                - item: CI/CD minutes according to tier purchased
                - item: Priority support
                - item: No use case restrictions
              cta:
                text: Get 20% discount
                url: /sales/
                data_ga_name: get discount
                data_ga_location: education feature tiers
    - name: 'education-stats'
      data:
        header: Be part of the GitLab community
        stats:
          - spotlight_text: 1,000+
            text: educational institutions are part of the GitLab for Education Program
          - spotlight_text: 3 million
            text: people (and counting) use GitLab at educational institutions
          - spotlight_text: '65'
            text: countries are part of the GitLab for Education Program
